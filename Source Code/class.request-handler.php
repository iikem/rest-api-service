	<?php

		class RESTRequest
		{
			public $url_elements;
			public $verb;
			public $parameters;
			public $format;
			
			public function __construct() 
			{
				$uri=explode('?',$_SERVER['REQUEST_URI']);
				$uri=$uri[0];
				$uri=array_values(array_filter(explode('/',$uri),'strlen'));
				//unset($uri[0]);
				$uri = array_values($uri);
			
				$this->verb = $_SERVER['REQUEST_METHOD'];
				$this->url_elements =$uri;
					
				$content_type = false;
				if (isset($_SERVER['QUERY_STRING'])) parse_str($_SERVER['QUERY_STRING'], $this->parameters);   
				if(isset($_SERVER['CONTENT_TYPE']))$content_type = $_SERVER['CONTENT_TYPE'];   	
				switch($content_type) 
				{
					case "application/json":
						$body = file_get_contents("php://input");
						$body_params = json_decode($body);
						if($body_params) 
							foreach($body_params as $param_name => $param_value)$this->parameters[$param_name] = $param_value;              
								$this->format = "json";
					break;
					default:
					break;
				}    	
				return true;
			}
		}

	?>