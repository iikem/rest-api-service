	<?php

		require_once ('class.request-handler.php');
		require_once ('class.get-actions.php');
		require_once ('class.put-actions.php');
		require_once ('class.post-actions.php');
		require_once ('class.delete-actions.php');

		class RESTAPI
		{
			public $request;
			public $GET;
			public $POST;
			public $PUT;
			public $DELETE;

			public function __construct() 
			{
				$this->request 	= new RESTRequest();
				
				$this->GET 			= new GetActions();
				$this->POST 		= new PostActions();
				$this->PUT 			= new PutActions();
				$this->DELETE 		= new DeleteActions();

				foreach($this->request->url_elements as $arg)
					$func=$func.'_'.$arg;

				switch ($this->request->verb) {
					case 'GET':
						if((int)method_exists($this->GET,$func) > 0)	
							$this->GET->$func();	
						break;
					case 'POST':
						if((int)method_exists($this->POST,$func) > 0)	
							$this->POST->$func();	
						break;
					case 'PUT':
						if((int)method_exists($this->PUT,$func) > 0)	
							$this->PUT->$func();
						break;
					case 'DELETE':
						if((int)method_exists($this->DELETE,$func) > 0)	
							$this->DELETE->$func();	
						break	;				
					default:
						header("HTTP/1.0 404 Not Found");
						break;
				}

				header("HTTP/1.0 404 Not Found");
				exit;
			}
		}
	?>